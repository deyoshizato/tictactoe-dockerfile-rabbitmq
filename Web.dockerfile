FROM nginx:alpine
ADD ./web /var/www/jogovelha
ADD ./web/nginx.conf /etc/nginx/
WORKDIR /var/www/jogovelha
EXPOSE 9000