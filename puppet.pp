package { 'docker':
  ensure=> present,
}
package { 'docker-compose':
  ensure=> present,
}
package { 'puppetlabs-stdlib':
  ensure=> present,
}
service { 'docker':
  ensure=> running,
  enable=> true,
  name=> 'docker',
}
# exec{ 'stopando-docker-compose':{
#   command=> '/usr/bin/docker-compose down',
#   require=> [Service['docker'], Package['docker-compose'], Package['docker']]
# }
# exec{ 'excutando-docker-compose':
#   command=> '/usr/bin/docker-compose up -d --build',
#   require=> [Service['docker'], Package['docker-compose'], Package['docker']]
# }
