FROM java:latest
ADD ./jogador/target/*.jar /jogador/app.jar
WORKDIR /jogador
EXPOSE 8082

CMD sleep 20 && java -jar app.jar
