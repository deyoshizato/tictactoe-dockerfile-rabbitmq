#!/bin/bash
echo "[INFO] Iniciando tests..."

pushd ./jogador > /dev/null
echo "[INFO] Testing `pwd`..."
mvn test > /dev/null 
popd > /dev/null

pushd ./placar > /dev/null
echo "[INFO] Testing `pwd`..."
mvn test > /dev/null 
popd > /dev/null

pushd ./monolito > /dev/null
echo "[INFO] Testing `pwd`..."
mvn test > /dev/null 
popd > /dev/null

pushd ./receiver > /dev/null
echo "[INFO] Testing `pwd`..."
mvn test > /dev/null 
popd > /dev/null

echo "[INFO] Tests terminados..."
