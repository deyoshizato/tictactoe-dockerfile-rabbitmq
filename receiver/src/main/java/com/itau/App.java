package com.itau;

import com.rabbitmq.client.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeoutException;

public class App
{
    private final static String QUEUE_NAME = "fila_da_mae";

    public static void main( String[] args ) throws IOException, TimeoutException
    {
        System.getProperties().put("server.port", "8083");


        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Aguardando mensagens. Pressione CTRL+C para sair");


        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = " [x] Recebeu: '" + new String(body, "UTF-8") + "'";

                gravar(Paths.get("/var/log/output.log"), message);
                System.out.println(message);
            }
        };

        while(true)
            channel.basicConsume(QUEUE_NAME, true, consumer);
    }

    public static void gravar(Path p, String message){
        try {
            StringBuilder log = new StringBuilder();
            new File("/var/log/output.log").createNewFile();
            for (String line : Files.readAllLines(p)){
                log.append(line + "\n");
            }
            log.append(message + "\n");
            Files.write(p, log.toString().getBytes());
        } catch (IOException e) {
            System.out.println("Não foi possível gravar o arquivo");
        }

    }

}
