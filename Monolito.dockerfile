FROM java:latest
ADD ./monolito/target/*.jar /monolito/app.jar
WORKDIR /monolito
EXPOSE 8080
# CMD sleep 40 && java -jar app.jar

CMD sleep 40 && java -jar app.jar