package com.itau.Jogador.Controller;

import com.itau.Jogador.mq.Evento;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.itau.Jogador.Model.Jogador;

import java.util.ArrayList;
import java.util.List;


@RestController
public class JogadorController {

    List<Jogador> jogadores = new  ArrayList<Jogador>();

    @RequestMapping(path="/jogador", method=RequestMethod.POST)
    @CrossOrigin
    public @ResponseBody ResponseEntity<?> buscar(@RequestBody String nome) {
        try {
            Evento.send("g4.ms.Jogador.JogadorController.buscar");
            for (Jogador j : jogadores){
                if (j.getNome().equals(nome))
                    return ResponseEntity.ok(j);
            }
            Jogador novo = new Jogador(nome);
            jogadores.add(novo);
            return ResponseEntity.ok(novo);
        } catch (Exception e) {
            return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getStackTrace());
        }
    }

    @RequestMapping(path = "/jogador/todos")
    @CrossOrigin
    public @ResponseBody ResponseEntity<?> buscarTodos(){
        Evento.send("g4.ms.Jogador.JogadorController.buscarTodos");

        return ResponseEntity.ok(jogadores);
    }

    @RequestMapping(path="/jogador/pontos/incrementar", method=RequestMethod.POST)
    @CrossOrigin
    public @ResponseBody ResponseEntity<?> incrementaPontuacao(@RequestBody int posicao) {
        try {
            Evento.send("g4.ms.Jogador.JogadorController.incrementaPontuacao");

            jogadores.get(posicao).incrementarPontos();
            return ResponseEntity.ok(jogadores);
        } catch (Exception e) {
            return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getStackTrace());
        }
    }
}
