FROM java:latest
ADD ./placar/target/*.jar /placar/app.jar
WORKDIR /placar
EXPOSE 8081

CMD sleep 20 && java -jar app.jar
