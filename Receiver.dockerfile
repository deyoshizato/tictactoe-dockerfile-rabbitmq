FROM java:latest
ADD ./receiver/target/*.jar /receiver/app.jar
WORKDIR /receiver
VOLUME "/var/log" 
EXPOSE 8083

RUN curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-6.2.4-amd64.deb
RUN dpkg -i filebeat-6.2.4-amd64.deb
COPY filebeat.yml /etc/filebeat/filebeat.yml

CMD /etc/init.d/filebeat start && sleep 40 && java -jar app.jar && tail -f /var/log/output.log