package com.itau.requester;

import com.github.kevinsawicki.http.HttpRequest;

public class ApiRequester {
    public static String get(String baseUrl) {
        return HttpRequest
                .get(baseUrl)
                .header("Content-type", "application/json")
                .body();
    }
}
