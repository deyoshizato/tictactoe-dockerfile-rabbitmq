package com.itau;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
        System.getProperties().put("server.port", "8081");
        SpringApplication.run(App.class, args);
    }
}
