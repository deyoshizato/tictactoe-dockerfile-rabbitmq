package com.itau.jogovelha.controller;

import com.itau.jogovelha.model.Jogada;
import com.itau.jogovelha.model.Rodada;
import com.itau.jogovelha.mq.Evento;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin
public class JogoController {

	Rodada rodada = new Rodada("Jogador 1", "Jogador 2");

	@RequestMapping("/")
	public @ResponseBody
    ResponseEntity<?> getPlacar() {
        Evento.send("g4.monolito.JogoController.getPlacar");
		return ResponseEntity.ok(rodada.getPlacar());
	}

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<?> jogar(@RequestBody Jogada jogada) {
        Evento.send("g4.monolito.JogoController.jogar");
        rodada.jogar(jogada.x, jogada.y);
        return ResponseEntity.ok(rodada.getPlacar());
    }

    @RequestMapping("/iniciar")
    public @ResponseBody
    ResponseEntity<?> iniciarJogo() {
        Evento.send("g4.monolito.JogoController.iniciarJogo");
        rodada.iniciarJogo();
        return ResponseEntity.ok(rodada.getPlacar());
    }
}
