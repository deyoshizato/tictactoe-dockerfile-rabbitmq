#!/bin/bash
echo "[INFO] Criando pasta build..."
rm -rf build > /dev/null
mkdir -p build > /dev/null

echo "[INFO] Iniciando builds..."

pushd ./jogador > /dev/null
echo "[INFO] Building `pwd`..."
mvn package > /dev/null 
popd > /dev/null
pushd ./build > /dev/null
mkdir -p jogador/target > /dev/null
cp ../jogador/target/*.jar jogador/target/app.jar > /dev/null
popd > /dev/null

pushd ./placar > /dev/null
echo "[INFO] Building `pwd`..."
mvn package > /dev/null 
popd > /dev/null
pushd ./build > /dev/null
mkdir -p placar/target > /dev/null
cp ../placar/target/*.jar placar/target/app.jar > /dev/null
popd > /dev/null

pushd ./monolito > /dev/null
echo "[INFO] Building `pwd`..."
mvn package > /dev/null 
popd > /dev/null
pushd ./build > /dev/null
mkdir -p monolito/target > /dev/null
cp ../monolito/target/*.jar monolito/target/app.jar > /dev/null
popd > /dev/null

pushd ./receiver > /dev/null
echo "[INFO] Building `pwd`..."
mvn package > /dev/null 
popd > /dev/null
pushd ./build > /dev/null
mkdir -p receiver/target > /dev/null
cp ../receiver/target/*.jar receiver/target/app.jar > /dev/null
popd > /dev/null

echo "[INFO] Builds terminados..."

echo "[INFO] Copiando arquivos para pasta build..."
cp puppet.pp build/ > /dev/null
cp docker-compose.yml build/ > /dev/null
cp Jogador.dockerfile build/ > /dev/null
cp Placar.dockerfile build/ > /dev/null
cp Monolito.dockerfile build/ > /dev/null
cp Receiver.dockerfile build/ > /dev/null
cp Web.dockerfile build/ > /dev/null
cp filebeat.yml build/ > /dev/null
cp -r ./web build/ > /dev/null 
cp -r ./elasticsearch build/ > /dev/null 
cp -r ./kibana build/ > /dev/null
cp -r ./logstash build/ > /dev/null


